import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss'],
})
export class ViewComponent implements OnInit {
  articleTitle!: string | null;
  articleAuthor!: string | null;
  articleText!: string | null;
  cardText!: string;

  constructor(private router: Router) {
    this.cardText =
      'Essa é apenas uma pré visualização de como o seu conteúdo ficará após ser publicado!';
  }

  ngOnInit(): void {
    this.articleTitle = localStorage.getItem('articleTitle');
    this.articleAuthor = localStorage.getItem('articleAuthor');
    this.articleText = localStorage.getItem('articleText');
  }

  goToForm(isEdit: boolean): void {
    if (!isEdit) {
      localStorage.clear();
    }
    this.router.navigate(['']);
  }
}
