import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  contentCard!: string;
  articleTitle!: string;
  articleAuthor!: string;
  articleText!: string;

  constructor(
    private router: Router
  ) {
    this.contentCard = 'Esse formulário é responsável pela criação de um novo artigo. Preencha os campos com atenção!';
   }

  ngOnInit(): void {
    this.articleTitle = localStorage.getItem('articleTitle') || '';
    this.articleAuthor = localStorage.getItem('articleAuthor') || '';
    this.articleText = localStorage.getItem('articleText') || '';
  }

  publish(): void {
    localStorage.setItem('articleText', this.articleText);
    localStorage.setItem('articleTitle', this.articleTitle);
    localStorage.setItem('articleAuthor', this.articleAuthor);

    this.router.navigate(['/previa']);
  }

}
