import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit {
  @Input() title!: string;
  @Input() customTitle!: string;
  @Input() content!: string;
  @Input() type!: string;

  icon!: string;

  constructor() {
    this.type = 'alert';
    this.icon = 'info';
    this.title = '';
    this.content = '';
  }

  ngOnInit(): void {
    if (this.customTitle) {
      this.title = this.customTitle;
    } else {
      this.title = this.getTitleByType(this.type);
    }
  }

  getTitleByType(type: string): string {
    switch (type) {
      case 'danger':
        this.icon = 'warning';
        return 'Cuidado!';
        break;
      default:
        return 'Atenção!';
        break;
    }
  }
}
